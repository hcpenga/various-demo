package com.example.variousdemo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author vicente
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/testone")
public class TestoneController {

}
